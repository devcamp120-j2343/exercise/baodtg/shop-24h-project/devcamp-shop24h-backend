const express = require("express");
const path = require('path')
const cors = require('cors');
var mongoose = require('mongoose');

//Khai báo model mongoose:
const productTypeModel = require("./apps/models/productType.model");
const productModel = require("./apps/models/product.model");
const customerModel = require("./apps/models/customer.model");
const orderModel = require("./apps/models/order.model");
const orderDetailModel = require("./apps/models/orderDetail.model");



//import router
const { productTypeRouter } = require("./apps/routers/productType.routes");
const { productRouter } = require("./apps/routers/product.routes");
const { customerRouter } = require("./apps/routers/customer.routes");
const { orderRouter } = require("./apps/routers/order.routes");
const { orderDetailRouter } = require("./apps/routers/orderDetail.routes");


const app = express();

app.use(express.json())


//kết nối mongoDB
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Shop24h")
    .then(() => console.log("Connected to Mongo Successfully"))
    .catch(error => handleError(error))

const PORT = process.env.ENV_PORT || 8000;


app.use("/api/", productTypeRouter)
app.use("/api/", productRouter)
app.use("/api/", customerRouter)
app.use("/api/", orderRouter)
app.use("/api/", orderDetailRouter)


app.listen(PORT, () => {
    console.log(`App listening on port ${PORT}`)
})

module.exports = app;