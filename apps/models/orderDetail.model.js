const mongoose = require('mongoose');

const orderDetailModel = new mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    product: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "product"
    },
    quantity: {
        type: Number,
        default: 0
    }
})
module.exports = mongoose.model('OrderDetail', orderDetailModel)