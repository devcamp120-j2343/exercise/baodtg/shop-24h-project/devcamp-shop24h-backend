const mongoose = require('mongoose');


const productTypeModel = new mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    name: {
        type: String,
        unique: true,
        required: true
    },
    description: {
        type: String
    }

})
module.exports = mongoose.model('ProductType', productTypeModel)