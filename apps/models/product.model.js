const mongoose = require('mongoose');

const productModel = new mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    name: {
        type: String,
        unique: true,
        required: true
    },
    description: {
        type: String
    },
    type: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "ProductType",
        required: true
    },
    imageUrl: {
        type: String,
        required: true
    },
    buyPrice: {
        type: Number,
        required: true
    },
    promotionPrice: {
        type: Number,
        required: true
    },
    amount: {
        type: Number,
        default: 0
    }
})
module.exports = mongoose.model('product', productModel)