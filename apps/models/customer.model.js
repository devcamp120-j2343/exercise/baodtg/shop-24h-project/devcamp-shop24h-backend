const mongoose = require('mongoose');

const customerModel = new mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        required: true
    },
    phone: {
        type: String
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    address: {
        type: String,
        default: ""
    },
    city: {
        type: String,
        default: ""
    },
    country: {
        type: String,
        default: ""
    },
    orders: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Order"
    }]
})

module.exports = mongoose.model('customer', customerModel)