const mongoose = require('mongoose');

const orderModel = new mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    orderDate: {
        type: Date,
        default: new Date()
    },
    shippedDate: {
        type: Date,
    },
    note: {
        type: String
    },
    orderDetails: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "OrderDetail"
    },
    cost: {
        type: Number,
        default: 0,
        required: true
    }
}) 

module.exports = mongoose.model('order', orderModel)