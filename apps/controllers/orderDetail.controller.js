const mongoose = require('mongoose')

const orderDetailModel = require('../models/orderDetail.model');
//Create new OrderDetail:
const createOrderDetail = async (req, res) => {
    console.log("Create OrderDetail...");
    //B1: thu thập dữ liệu:
    let { OrderDetail, quantity } = req.body
    //B2: kiểm tra dữ liệu:

    //B3: khoi tao model:
    let newOrderDetailDataObj = new orderDetailModel({
        _id: new mongoose.Types.ObjectId(),
        OrderDetail,
        quantity,

    })
    try {
        const newOrderDetail = await orderDetailModel.create(newOrderDetailDataObj);
        if (newOrderDetail) {
            return res.status(201).json({
                status: `Create new OrderDetail successfully`,
                data: newOrderDetail
            })
        }

    } catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}

//Get all OrderDetails
const getAllOrderDetail = async (req, res) => {

    try {
        const orderDetailList = await orderDetailModel.find();
        if (orderDetailList && orderDetailList.length > 0) {
            return res.status(200).json({
                status: `Get all OrderDetails successfully !`,
                data: orderDetailList
            })
        } else {
            return res.status(404).json({
                status: `Not found any OrderDetail!`,
                data: orderDetailList
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

//get OrderDetail by Id
const getOrderDetailById = async (req, res) => {
    //collect data:
    const orderDetailId = req.params.orderDetailId;
    //validate data
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `OrderDetail Id ${orderDetailId} is invalid!`
        })
    }
    try {
        const orderDetailFoundById = await orderDetailModel.findById(orderDetailId);
        if (orderDetailFoundById) {
            return res.status(200).json({
                status: `Get OrderDetail by Id ${orderDetailId} successfully !`,
                data: orderDetailFoundById
            })
        } else {
            return res.status(404).json({
                status: `Not found any OrderDetails by Id ${orderDetailId} !`,
                data: orderDetailFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

//update order detail by id
const updateOrderDetailById = async (req, res) => {
    //collect data:
    const orderDetailId = req.params.orderDetailId;
    let { OrderDetail, quantity } = req.body

    //validate:
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `OrderDetail Type Id ${orderDetailId} is invalid!`
        })
    }
   

    const orderDetailUpdateData = {
        OrderDetail,
        quantity,
    }
    //use model
    try {
        const orderDetailUpdated = await orderDetailModel.findByIdAndUpdate(orderDetailId, orderDetailUpdateData);
        if (orderDetailUpdated) {
            const newOrderDetailUpdated = await orderDetailModel.findOne({ _id: orderDetailId })
            return res.status(200).json({
                status: `Update OrderDetail by id ${orderDetailId} successfully`,
                data: newOrderDetailUpdated
            })
        } else {
            return res.status(404).json({
                status: `Not found any OrderDetails by Id ${orderDetailId} !`,
                data: orderDetailUpdated
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

//delete order detail by id
const deleteOrderDetailById = async (req, res) => {
    //collect data:
    const orderDetailId = req.params.orderDetailId;

    //validate:
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `OrderDetail Id ${orderDetailId} is invalid!`
        })
    }
    try {
        const orderDetailDeleted = await orderDetailModel.findByIdAndDelete(orderDetailId);
        if (orderDetailDeleted) {
            return res.status(200).json({
                status: `Delete OrderDetail by id ${orderDetailId} successfully`,
            })
        } else {
            return res.status(404).json({
                status: `Not found any OrderDetails by Id ${orderDetailId} !`,
                data: orderDetailDeleted
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

module.exports = {
    createOrderDetail,
    getAllOrderDetail,
    getOrderDetailById,
    updateOrderDetailById,
    deleteOrderDetailById
}