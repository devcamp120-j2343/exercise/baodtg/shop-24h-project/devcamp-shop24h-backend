const productTypeModel = require('../models/productType.model');

const mongoose = require('mongoose')

//Create new productType:
const createProductType = async (req, res) => {
    console.log("Create product Type");
    //B1: thu thập dữ liệu:
    let { name, description } = req.body
    //B2: kiểm tra dữ liệu:
    if (!name) {
        return res.status(400).json({
            message: `name is required`,
        })
    }

    //B3: khoi tao model:
    let newProductTypeDataObj = new productTypeModel({
        _id: new mongoose.Types.ObjectId(),
        name,
        description
    })
    try {
        const newProductType = await productTypeModel.create(newProductTypeDataObj);
        if (newProductType) {
            return res.status(201).json({
                status: `Create new ProductType successfully`,
                data: newProductType
            })
        }

    } catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }


}

//Get all ProductType
const getAllProductType = async (req, res) => {

    try {
        const productTypeList = await productTypeModel.find();
        if (productTypeList && productTypeList.length > 0) {
            return res.status(200).json({
                status: `Get all productType successfully !`,
                data: productTypeList
            })
        } else {
            return res.status(404).json({
                status: `Not found any productType!`,
                data: productTypeList
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

const getProductTypeById = async (req, res) => {
    //collect data:
    const productTypeId = req.params.productTypeId;
    //validate data
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `Product Type Id ${productTypeId} is invalid!`
        })
    }
    try {
        const productTypeFoundById = await productTypeModel.findById(productTypeId);
        if (productTypeFoundById) {
            return res.status(200).json({
                status: `Get product Type by Id ${productTypeId} successfully !`,
                data: productTypeFoundById
            })
        } else {
            return res.status(404).json({
                status: `Not found any products Type by Id ${productTypeId} !`,
                data: productTypeFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

const updateProductTypeById = async (req, res) => {
    //collect data:
    const productTypeId = req.params.productTypeId;
    const { name, description } = req.body;

    //validate:
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `Product Type Id ${productTypeId} is invalid!`
        })
    }
    if (!name) {
        return res.status(400).json({
            status: 'Bad request',
            message: `name id required`
        })
    }

    const productTypeUpdateData = {
        name,
        description
    }
    //use model
    try {
        const productTypeUpdated = await productTypeModel.findByIdAndUpdate(productTypeId, productTypeUpdateData);
        if (productTypeUpdated) {
            const newProductTypeUpdated = await productTypeModel.findOne({ name })
            return res.status(200).json({
                status: `Update product by id ${productTypeId} successfully`,
                data: newProductTypeUpdated
            })
        } else {
            return res.status(404).json({
                status: `Not found any products by Id ${productTypeId} !`,
                data: productFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

const deleteProductTypeById = async (req, res) => {
    //collect data:
    const productTypeId = req.params.productTypeId;

    //validate:
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `Product Type Id ${productTypeId} is invalid!`
        })
    }
    try {
        const productTypeDeleted = await productTypeModel.findByIdAndDelete(productTypeId);
        if (productTypeDeleted) {
            return res.status(200).json({
                status: `Delete product type by id ${productTypeId} successfully`,
            })
        } else {
            return res.status(404).json({
                status: `Not found any products by Id ${productTypeId} !`,
                data: productTypeDeleted
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }

}


module.exports = {
    createProductType,
    getAllProductType,
    getProductTypeById,
    updateProductTypeById,
    deleteProductTypeById
}