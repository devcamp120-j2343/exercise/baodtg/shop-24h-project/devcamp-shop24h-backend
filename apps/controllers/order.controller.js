const mongoose = require('mongoose')

const orderModel = require('../models/order.model');

//Create new Order:
const createOrder = async (req, res) => {
    console.log("Create Order...");
    //B1: thu thập dữ liệu:
    let { shippedDate, note, orderDetails } = req.body
    //B2: kiểm tra dữ liệu:

    //B3: khoi tao model:
    let newOrderDataObj = new orderModel({
        _id: new mongoose.Types.ObjectId(),
        shippedDate,
        note,
        orderDetails

    })
    try {
        const newOrder = await orderModel.create(newOrderDataObj);
        if (newOrder) {
            return res.status(201).json({
                status: `Create new Order successfully`,
                data: newOrder
            })
        }

    } catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}

//Get all Orders
const getAllOrder = async (req, res) => {

    try {
        const OrderList = await orderModel.find();
        if (OrderList && OrderList.length > 0) {
            return res.status(200).json({
                status: `Get all Orders successfully !`,
                data: OrderList
            })
        } else {
            return res.status(404).json({
                status: `Not found any Order!`,
                data: OrderList
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

//get Order by Id
const getOrderById = async (req, res) => {
    //collect data:
    const orderId = req.params.orderId;
    //validate data
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `Order Id ${orderId} is invalid!`
        })
    }
    try {
        const orderFoundById = await orderModel.findById(orderId);
        if (orderFoundById) {
            return res.status(200).json({
                status: `Get Order by Id ${orderId} successfully !`,
                data: orderFoundById
            })
        } else {
            return res.status(404).json({
                status: `Not found any Orders by Id ${orderId} !`,
                data: orderFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

const updateOrderById = async (req, res) => {
    //collect data:
    const orderId = req.params.orderId;
    let { shippedDate, note, orderDetails } = req.body

    //validate:
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `Order Type Id ${orderId} is invalid!`
        })
    }


    const orderUpdateData = {
        shippedDate,
        note,
        orderDetails
    }
    //use model
    try {
        const orderUpdated = await orderModel.findByIdAndUpdate(orderId, orderUpdateData);
        if (orderUpdated) {
            const newOrderUpdated = await orderModel.findOne({ _id: orderId })
            return res.status(200).json({
                status: `Update Order by id ${orderId} successfully`,
                data: newOrderUpdated
            })
        } else {
            return res.status(404).json({
                status: `Not found any Orders by Id ${orderId} !`,
                data: OrderFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

const deleteOrderById = async (req, res) => {
    //collect data:
    const orderId = req.params.orderId;

    //validate:
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `Order Id ${orderId} is invalid!`
        })
    }
    try {
        const orderDeleted = await orderModel.findByIdAndDelete(orderId);
        if (orderDeleted) {
            return res.status(200).json({
                status: `Delete Order by id ${orderId} successfully`,
            })
        } else {
            return res.status(404).json({
                status: `Not found any Orders by Id ${orderId} !`,
                data: orderDeleted
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

module.exports = {
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrderById,
    deleteOrderById
}