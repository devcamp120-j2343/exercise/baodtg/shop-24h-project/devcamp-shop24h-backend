const mongoose = require('mongoose')

const productModel = require('../models/product.model');

//Create new product:
const createProduct = async (req, res) => {
    console.log("Create product...");
    //B1: thu thập dữ liệu:
    let { name, description, type, imageUrl, buyPrice, promotionPrice, amount } = req.body
    //B2: kiểm tra dữ liệu:
    if (!name) {
        return res.status(400).json({
            message: `name is required`,
        })
    }

    if (!type) {
        return res.status(400).json({
            message: `type is required`,
        })
    }
    if (!imageUrl) {
        return res.status(400).json({
            message: `imageUrl is required`,
        })
    }
    if (!buyPrice) {
        return res.status(400).json({
            message: `buyPrice is required`,
        })
    }
    if (!promotionPrice) {
        return res.status(400).json({
            message: `promotionPrice is required`,
        })
    }

    //B3: khoi tao model:
    let newProductDataObj = new productModel({
        _id: new mongoose.Types.ObjectId(),
        name,
        description,
        type,
        imageUrl,
        buyPrice,
        promotionPrice,
        amount
    })
    try {
        const newProduct = await productModel.create(newProductDataObj);
        if (newProduct) {
            return res.status(201).json({
                status: `Create new Product successfully`,
                data: newProduct
            })
        }

    } catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}

//Get all Products
const getAllProduct = async (req, res) => {

    try {
        const productList = await productModel.find();
        if (productList && productList.length > 0) {
            return res.status(200).json({
                status: `Get all products successfully !`,
                data: productList
            })
        } else {
            return res.status(404).json({
                status: `Not found any product!`,
                data: productList
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

//get product by Id
const getProductById = async (req, res) => {
    //collect data:
    const productId = req.params.productId;
    //validate data
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `Product Id ${productId} is invalid!`
        })
    }
    try {
        const productFoundById = await productModel.findById(productId);
        if (productFoundById) {
            return res.status(200).json({
                status: `Get product by Id ${productId} successfully !`,
                data: productFoundById
            })
        } else {
            return res.status(404).json({
                status: `Not found any products by Id ${productId} !`,
                data: productFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

const updateProductById = async (req, res) => {
    //collect data:
    const productId = req.params.productId;
    let { name, description, type, imageUrl, buyPrice, promotionPrice, amount } = req.body

    //validate:
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `Product Type Id ${productId} is invalid!`
        })
    }
    if (!name) {
        return res.status(400).json({
            status: 'Bad request',
            message: `name id required`
        })
    }
    if (!type) {
        return res.status(400).json({
            message: `type is required`,
        })
    }
    if (!imageUrl) {
        return res.status(400).json({
            message: `imageUrl is required`,
        })
    }
    if (!buyPrice) {
        return res.status(400).json({
            message: `buyPrice is required`,
        })
    }
    if (!promotionPrice) {
        return res.status(400).json({
            message: `promotionPrice is required`,
        })
    }

    const productUpdateData = {
        name,
        description,
        type,
        imageUrl,
        buyPrice,
        promotionPrice,
        amount
    }
    //use model
    try {
        const productUpdated = await productModel.findByIdAndUpdate(productId, productUpdateData);
        if (productUpdated) {
            const newProductUpdated = await productModel.findOne({ name })
            return res.status(200).json({
                status: `Update product by id ${productId} successfully`,
                data: newProductUpdated
            })
        } else {
            return res.status(404).json({
                status: `Not found any products by Id ${productId} !`,
                data: productFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

const deleteProductById = async (req, res) => {
    //collect data:
    const productId = req.params.productId;

    //validate:
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `Product Id ${productId} is invalid!`
        })
    }
    try {
        const productDeleted = await productModel.findByIdAndDelete(productId);
        if (productDeleted) {
            return res.status(200).json({
                status: `Delete product by id ${productId} successfully`,
            })
        } else {
            return res.status(404).json({
                status: `Not found any products by Id ${productId} !`,
                data: productDeleted
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}
module.exports = {
    createProduct,
    getAllProduct,
    getProductById,
    updateProductById,
    deleteProductById
}
