const mongoose = require('mongoose')

const customerModel = require('../models/customer.model');


//Create new Customer:
const createCustomer = async (req, res) => {
    console.log("Create Customer...");
    //B1: thu thập dữ liệu:
    let { fullName, phone, email, address, city, country, orders } = req.body
    //B2: kiểm tra dữ liệu:
    if (!fullName) {
        return res.status(400).json({
            message: `fullName is required`,
        })
    }
    if (!phone) {
        return res.status(400).json({
            message: `phone is required`,
        })
    }
    if (!email) {
        return res.status(400).json({
            message: `email is required`,
        })
    }
    

    //B3: khoi tao model:
    let newCustomerDataObj = new customerModel({
        _id: new mongoose.Types.ObjectId(),
        fullName,
        phone,
        email,
        address,
        city,
        country,
        orders
    })
    try {
        const newCustomer = await customerModel.create(newCustomerDataObj);
        if (newCustomer) {
            return res.status(201).json({
                status: `Create new Customer successfully`,
                data: newCustomer
            })
        }

    } catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}

//Get all Customer
const getAllCustomer = async (req, res) => {

    try {
        const customerList = await customerModel.find();
        if (customerList && customerList.length > 0) {
            return res.status(200).json({
                status: `Get all Customers successfully !`,
                data: customerList
            })
        } else {
            return res.status(404).json({
                status: `Not found any Customers!`,
                data: customerList
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

//get Customer by Id
const getCustomerById = async (req, res) => {
    //collect data:
    const customerId = req.params.customerId;
    //validate data
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `Customer Id ${customerId} is invalid!`
        })
    }
    try {
        const CustomerFoundById = await customerModel.findById(customerId);
        if (CustomerFoundById) {
            return res.status(200).json({
                status: `Get Customer by Id ${customerId} successfully !`,
                data: CustomerFoundById
            })
        } else {
            return res.status(404).json({
                status: `Not found any Customers by Id ${customerId} !`,
                data: CustomerFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

//Update customer by id
const updateCustomerById = async (req, res) => {
    //collect data:
    const customerId = req.params.customerId;
    let { fullName, phone, email, address, city, country, orders } = req.body

    //validate:
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `Customer Id ${customerId} is invalid!`
        })
    }
    if (!fullName) {
        return res.status(400).json({
            message: `fullName is required`,
        })
    }
    if (!phone) {
        return res.status(400).json({
            message: `phone is required`,
        })
    }
    if (!email) {
        return res.status(400).json({
            message: `email is required`,
        })
    }

    const customerUpdateData = {
        fullName,
        phone,
        email,
        address,
        city,
        country,
        orders
    }
    //use model
    try {
        const customerUpdated = await customerModel.findByIdAndUpdate(customerId, customerUpdateData);
        if (customerUpdated) {
            const newCustomerUpdated = await customerModel.findOne({ email })
            return res.status(200).json({
                status: `Update Customer by id ${customerId} successfully`,
                data: newCustomerUpdated
            })
        } else {
            return res.status(404).json({
                status: `Not found any Customers by Id ${customerId} !`,
                data: CustomerFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

//delete customer by id

const deleteCustomerById = async (req, res) => {
    //collect data:
    const customerId = req.params.customerId;

    //validate:
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `Customer Id ${customerId} is invalid!`
        })
    }
    try {
        const customerDeleted = await customerModel.findByIdAndDelete(customerId);
        if (customerDeleted) {
            return res.status(200).json({
                status: `Delete Customer by id ${customerId} successfully`,
            })
        } else {
            return res.status(404).json({
                status: `Not found any Customers by Id ${customerId} !`,
                data: customerDeleted
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal Server Error',
            message: error.message
        })
    }
}

module.exports = {
    createCustomer,
    getAllCustomer,
    getCustomerById,
    updateCustomerById,
    deleteCustomerById
}