//Khai báo thư viện express
const express = require('express');
const { createCustomer, getAllCustomer, getCustomerById, updateCustomerById, deleteCustomerById } = require('../controllers/customer.controller');

//Tạo router:
const customerRouter = express.Router();

//router post customer
customerRouter.post("/customers", createCustomer )

//router get all customer
customerRouter.get("/customers", getAllCustomer)

//router get customer by id
customerRouter.get("/customers/:customerId", getCustomerById)

//router update customer by id
customerRouter.put("/customers/:customerId", updateCustomerById)

// router update customer by id
customerRouter.delete("/customers/:customerId", deleteCustomerById)



module.exports = { customerRouter }
