//Khai báo thư viện express
const express = require('express');
const { createProductType, getAllProductType, getProductTypeById, updateProductTypeById, deleteProductTypeById } = require('../controllers/productType.controller');

//Tạo router:
const productTypeRouter = express.Router();

//router post productType
productTypeRouter.post("/productTypes", createProductType)

//router get all productType
productTypeRouter.get("/productTypes", getAllProductType)

//router get productType by id
productTypeRouter.get("/productTypes/:productTypeId", getProductTypeById)

//router update productType by id
productTypeRouter.put("/productTypes/:productTypeId", updateProductTypeById)

// router update productType by id
productTypeRouter.delete("/productTypes/:productTypeId", deleteProductTypeById)

module.exports = { productTypeRouter }
