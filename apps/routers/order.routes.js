//Khai báo thư viện express
const express = require('express');
const { createOrder, getAllOrder, getOrderById, updateOrderById, deleteOrderById } = require('../controllers/order.controller');

//Tạo router:
const orderRouter = express.Router();

//router post order
orderRouter.post("/orders", createOrder )

//router get all order
orderRouter.get("/orders", getAllOrder)

//router get order by id
orderRouter.get("/orders/:orderId", getOrderById)

//router update order by id
orderRouter.put("/orders/:orderId", updateOrderById)

// router update order by id
orderRouter.delete("/orders/:orderId", deleteOrderById)



module.exports = { orderRouter }
