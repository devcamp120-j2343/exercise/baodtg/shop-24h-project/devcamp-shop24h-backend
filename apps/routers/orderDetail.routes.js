//Khai báo thư viện express
const express = require('express');
const { createOrderDetail, getAllOrderDetail, getOrderDetailById, updateOrderDetailById, deleteOrderDetailById } = require('../controllers/orderDetail.controller');

//Tạo router:
const orderDetailRouter = express.Router();

//router post orderDetail
orderDetailRouter.post("/orderDetails", createOrderDetail)

//router get all orderDetail
orderDetailRouter.get("/orderDetails", getAllOrderDetail)

//router get orderDetail by id
orderDetailRouter.get("/orderDetails/:orderDetailId", getOrderDetailById)

//router update orderDetail by id
orderDetailRouter.put("/orderDetails/:orderDetailId", updateOrderDetailById)

// router update orderDetail by id
orderDetailRouter.delete("/orderDetails/:orderDetailId", deleteOrderDetailById)



module.exports = { orderDetailRouter }
