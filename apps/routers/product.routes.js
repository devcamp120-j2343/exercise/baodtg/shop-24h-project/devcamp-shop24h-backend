//Khai báo thư viện express
const express = require('express');
const { createProduct, getAllProduct, getProductById, updateProductById, deleteProductById } = require('../controllers/product.controller');

//Tạo router:
const productRouter = express.Router();

//router post product
productRouter.post("/products", createProduct )

//router get all product
productRouter.get("/products", getAllProduct)

//router get product by id
productRouter.get("/products/:productId", getProductById)

//router update product by id
productRouter.put("/products/:productId", updateProductById)

// router update product by id
productRouter.delete("/products/:productId", deleteProductById)



module.exports = { productRouter }
